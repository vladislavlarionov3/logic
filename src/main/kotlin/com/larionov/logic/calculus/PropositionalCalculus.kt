package com.larionov.logic.calculus

import com.larionov.logic.alphabet.Sign
import com.larionov.logic.formula.*

fun axiom1(a: Formula<Sign>, b: Formula<Sign>) = a then (b then a)

fun axiom2(a: Formula<Sign>, b: Formula<Sign>, c: Formula<Sign>) =
    (a then (b then c)) then ((a then b) then (a then c))

fun axiom3(a: Formula<Sign>, b: Formula<Sign>) = (a and b) then a

fun axiom4(a: Formula<Sign>, b: Formula<Sign>) = (a and b) then b

fun axiom5(a: Formula<Sign>, b: Formula<Sign>) = a then (b then (a and b))

fun axiom6(a: Formula<Sign>, b: Formula<Sign>) = a then (a or b)

fun axiom7(a: Formula<Sign>, b: Formula<Sign>) = b then (a or b)

fun axiom8(a: Formula<Sign>, b: Formula<Sign>, c: Formula<Sign>) =
    (a then c) then ((b then c) then ((a or b) then c))

fun axiom9(a: Formula<Sign>, b: Formula<Sign>) = (a then b) then ((a then b.not()) then a.not())

fun axiom10(a: Formula<Sign>) = a.not().not() then a

fun <S : Sign> mp(aThenB: Then<out S>, a: Formula<S>) = aThenB.b.takeIf { aThenB.a == a }