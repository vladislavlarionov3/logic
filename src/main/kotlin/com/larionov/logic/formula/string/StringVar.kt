package com.larionov.logic.formula.string

import com.larionov.logic.alphabet.string.StringSing
import com.larionov.logic.alphabet.string.signOf
import com.larionov.logic.formula.Var

class StringVar(private val sing: StringSing) : Var {
    override fun toString() = "$sing"
}

fun varOf(value: String) = StringVar(signOf(value))