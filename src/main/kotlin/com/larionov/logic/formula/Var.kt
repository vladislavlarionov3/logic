package com.larionov.logic.formula

import com.larionov.logic.alphabet.Sign

/**
 * Пропозициональная переменная
 * Интерпретируется как высказывание
 */
interface Var : Sign, Formula<Sign>