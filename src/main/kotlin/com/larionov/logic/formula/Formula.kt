package com.larionov.logic.formula

import com.larionov.logic.alphabet.Sign
import com.larionov.logic.alphabet.Word

interface Formula<S : Sign> : Word<S>

class Not<S : Sign>(val formula: Formula<S>) : Formula<S> {

    override fun toString() = "!${formula.withBreaksIfNotVar()}"

    override fun equals(other: Any?) = when (other) {
        is Not<*> -> formula == other.formula
        else -> false
    }

    override fun hashCode(): Int {
        return formula.hashCode()
    }
}

class Or<S : Sign>(val a: Formula<S>, val b: Formula<S>) : Formula<S> {
    override fun toString() = "${a.withBreaksIfNotVar()} | ${b.withBreaksIfNotVar()}"

    override fun equals(other: Any?) = when (other) {
        is Or<*> -> a == other.a && b == other.b
        else -> false
    }

    override fun hashCode(): Int {
        var result = a.hashCode()
        result = 31 * result + b.hashCode()
        return result
    }
}

class And<S : Sign>(val a: Formula<S>, val b: Formula<S>) : Formula<S> {
    override fun toString() = "${a.withBreaksIfNotVar()} & ${b.withBreaksIfNotVar()}"

    override fun equals(other: Any?) = when (other) {
        is And<*> -> a == other.a && b == other.b
        else -> false
    }

    override fun hashCode(): Int {
        var result = a.hashCode()
        result = 31 * result + b.hashCode()
        return result
    }
}

class Then<S : Sign>(val a: Formula<S>, val b: Formula<S>) : Formula<S> {
    override fun toString() = "${a.withBreaksIfNotVar()} -> ${b.withBreaksIfNotVar()}"

    override fun equals(other: Any?) = when (other) {
        is Then<*> -> a == other.a && b == other.b
        else -> false
    }

    override fun hashCode(): Int {
        var result = a.hashCode()
        result = 31 * result + b.hashCode()
        return result
    }
}

class Breaks<S : Sign>(val formula: Formula<S>) : Formula<S> {
    override fun toString() = "($formula)"

    override fun equals(other: Any?) = when (other) {
        is Breaks<*> -> formula == other.formula
        else -> false
    }

    override fun hashCode(): Int {
        return formula.hashCode()
    }
}

fun <S : Sign> Formula<S>.not() = Not(this)

infix fun <S : Sign> Formula<S>.or(formula: Formula<S>) = Or(this, formula)

infix fun <S : Sign> Formula<S>.and(formula: Formula<S>) = And(this, formula)

infix fun <S : Sign> Formula<S>.then(formula: Formula<S>) = Then(this, formula)

fun <S : Sign> Formula<S>.withBreaks() = Breaks(this)

private fun <S : Sign> Formula<S>.withBreaksIfNotVar() = if (this is Var) this else withBreaks()
