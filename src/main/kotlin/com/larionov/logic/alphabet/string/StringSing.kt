package com.larionov.logic.alphabet.string

import com.larionov.logic.alphabet.Sign

data class StringSing(val value: String) : Sign {
    override fun toString() = value
}

fun signOf(value: String) = StringSing(value)