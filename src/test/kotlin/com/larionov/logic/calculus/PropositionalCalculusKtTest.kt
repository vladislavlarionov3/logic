package com.larionov.logic.calculus

import com.larionov.logic.alphabet.Sign
import com.larionov.logic.formula.Then
import com.larionov.logic.formula.and
import com.larionov.logic.formula.string.varOf
import com.larionov.logic.formula.then
import mu.KotlinLogging
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

var log = KotlinLogging.logger {}

internal class PropositionalCalculusKtTest {

    @Test
    fun axiom1() {
        val formula = axiom1(varOf("a"), varOf("b"))
        assertEquals("a -> (b -> a)", formula.toString())
    }

    @Test
    fun axiom2() {
        val formula = axiom2(varOf("a"), varOf("b"), varOf("c"))
        assertEquals("(a -> (b -> c)) -> ((a -> b) -> (a -> c))", formula.toString())
    }

    @Test
    fun axiom3() {
        val formula = axiom3(varOf("a"), varOf("b"))
        assertEquals("(a & b) -> a", formula.toString())
    }

    @Test
    fun axiom4() {
        val formula = axiom4(varOf("a"), varOf("b"))
        assertEquals("(a & b) -> b", formula.toString())
    }

    @Test
    fun axiom5() {
        val formula = axiom5(varOf("a"), varOf("b"))
        assertEquals("a -> (b -> (a & b))", formula.toString())
    }

    @Test
    fun axiom6() {
        val formula = axiom6(varOf("a"), varOf("b"))
        assertEquals("a -> (a | b)", formula.toString())
    }

    @Test
    fun axiom7() {
        val formula = axiom7(varOf("a"), varOf("b"))
        assertEquals("b -> (a | b)", formula.toString())
    }

    @Test
    fun axiom8() {
        val formula = axiom8(varOf("a"), varOf("b"), varOf("c"))
        assertEquals("(a -> c) -> ((b -> c) -> ((a | b) -> c))", formula.toString())
    }

    @Test
    fun axiom9() {
        val formula = axiom9(varOf("a"), varOf("b"))
        assertEquals("(a -> b) -> ((a -> (!b)) -> (!a))", formula.toString())
    }

    @Test
    fun axiom10() {
        val formula = axiom10(varOf("a"))
        assertEquals("(!(!a)) -> a", formula.toString())
    }

    @Test
    fun mp() {
        val a = varOf("a")
        val b = varOf("b")
        val formula = mp(a then b, a)
        assertEquals("b", formula.toString())
    }

    @Test
    fun example2_3() {
        log.info("Example 2.3")
        val p = varOf("P")
        val q = varOf("Q")
        log.info("Formulas:")
        val f1 = p then (q and p)
        val f2 = q then (q and p)
        val f3 = (p then (q and p)) then ((q then (q and p)) then ((p and q) then (q and p)))
        listOf(f1, f2, f3).forEach { log.info { it } }

        log.info("Calculus:")
        val mp1 = mp(f3, f1)
        log.info { "MP: $mp1" }
        val mp2 = mp(mp1 as Then<out Sign>, f2)
        log.info { "MP: $mp2" }

        assertEquals("(P & Q) -> (Q & P)", mp2.toString())
    }

    @Test
    fun example2_6() {
        log.info("Example 2.6")
        log.info("That A -> A:")
        val a = varOf("A")
        log.info("Formulas:")
        log.info { a }
        val b = a then a
        log.info { b }

        log.info("Calculus:")
        val f1 = axiom1(a, b)
        log.info { "axiom1: $f1" }
        val f2 = axiom2(a, b, a)
        log.info { "axiom2: $f2" }
        val f3 = mp(f2, f1)
        log.info { "MP: $f3" }
        val f4 = axiom1(a, a)
        log.info { "axiom1: $f4" }
        val f5 = mp(f3 as Then<out Sign>, f4)
        log.info { "MP: $f5" }

        assertEquals("A -> A", f5.toString())
    }
}