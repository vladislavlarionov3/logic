package com.larionov.logic.formula

import com.larionov.logic.formula.string.varOf
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class FormulaKtTest {
    @Test
    fun not() {
        val notA = varOf("a").not()
        Assertions.assertEquals("!a", notA.toString())
    }

    @Test
    fun or() {
        val aOrB = varOf("a") or varOf("b")
        Assertions.assertEquals("a | b", aOrB.toString())
    }

    @Test
    fun and() {
        val aAndB = varOf("a") and varOf("b")
        Assertions.assertEquals("a & b", aAndB.toString())
    }

    @Test
    fun then() {
        val aThenB = varOf("a") then varOf("b")
        Assertions.assertEquals("a -> b", aThenB.toString())
    }

    @Test
    fun withBreaks() {
        val aWithBreaks = varOf("a").withBreaks()
        Assertions.assertEquals("(a)", aWithBreaks.toString())
    }
}